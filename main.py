from utilites import parse_args
import openpyxl
import logging
import os, sys
from docx_decider import DocxClass
from pdf_decider import PdfClass


def set_logging(log_lev=None):
    logger = logging.getLogger()
    if log_lev == "NOTSET":
        log_lev_log = logging.NOTSET
    elif log_lev == "DEBUG":
        log_lev_log = logging.DEBUG
    elif log_lev == "INFO":
        log_lev_log = logging.INFO
    elif log_lev == "WARN":
        log_lev_log = logging.WARN
    elif log_lev == "ERROR":
        log_lev_log = logging.ERROR
    elif log_lev == "CRITICAL":
        log_lev_log = logging.CRITICAL
    else:
        log_lev_log = logging.NOTSET

    logging.getLogger().setLevel(log_lev_log)
    FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    # Add stdout handler, with level
    console = logging.StreamHandler(sys.stdout)
    console.setLevel(logging.INFO)
    formater = logging.Formatter(FORMAT)
    console.setFormatter(formater)
    logging.getLogger().addHandler(console)

    # Add file file handler, with level
    fileHandler = logging.FileHandler("debug.log", mode="w")
    fileHandler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(FORMAT)
    fileHandler.setFormatter(formatter)
    logging.getLogger().addHandler(fileHandler)


def load_xlsx_file_with_params(file):
    """
    load xlsx file with parameters
    !!! format is strong !!!
    KEYS   |   VALUES
    :param file:
    :return:
    """
    parameters = dict()
    wookbook = openpyxl.load_workbook(file)
    # Define variable to read the active sheet:
    worksheet = wookbook.active
    # Iterate the loop to read the cell values
    logging.debug("## Start read parametric xlsx file ################################")

    row_num = 0
    for row in worksheet.iter_rows(1, worksheet.max_row, values_only=True):
        # detect header
        if row_num == 0:
            pass
        else:
            if not row[0] == None and not row[0].startswith("#"):
                if len(row) < 3:
                    colour = 'green'
                else:
                    colour = row[2]
                parameters[row[0]] = (row[1], colour)
                logging.debug("       {0}:  {1}".format(row[0], row[1]))
        row_num += 1
    wookbook.close()
    logging.debug("##################################")
    return parameters


def validate_args(args):
    """
    checker arguments in command line
    :param args:
    :return:
    """
    if not args["input_file"] == None and not args['input_folder'] == None:
        raise Exception("You must select only one. Or input_file (-i) or input_folder (-f)")

    if args["input_file"] == None and args['input_folder'] == None:
        raise Exception("You must select one. input_file (-i) or input_folder (-f)")


def worker(args):
    """
    Main work function
    :param args:
    :return:
    """
    # get file list with one or all files from folder
    files_list = list()
    parameters = load_xlsx_file_with_params(args["params_xlsx_file"])
    if not args["input_file"] == None:
        files_list.append(args["input_file"])

    elif not args['input_folder'] == None:
        for file in os.listdir(args['input_folder']):
            full_file_name = os.path.join(args['input_folder'],file)
            if os.path.isfile(full_file_name):
                if '.pdf' in full_file_name or '.docx' in full_file_name:
                    files_list.append(full_file_name)
                    logging.debug(" add file {}".format(full_file_name))

    # by list files execute solution class
    for file in files_list:
        if '.docx' in file:
            solutionClass = DocxClass(file, parameters)
        elif '.pdf' in file:
            solutionClass = PdfClass(file, parameters)
        else:
            raise Exception("Not have solution for file {}".format(file))

        solutionClass.run()
        solutionClass.save(file, args["output_file"], args["output_folder"])


if __name__ == "__main__":
    try:
        args = parse_args()
        set_logging(args["log_level"])

        logging.debug("## Command Arguments #################################################")
        for arg in args:
            logging.debug("      {0}: {1}".format(arg, args[arg]))
        logging.debug("######################################################################")

        validate_args(args)
        worker(args)
    except Exception as ex:
        logging.exception("")
        raise ex
