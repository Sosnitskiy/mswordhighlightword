
work with python 3.9 + Windows 11

   1. download python and install from:
      
    https://www.python.org/downloads/

   3. download git and install:
    
    https://git-scm.com/download/win

   4. create folders for any external project

        cmd

        mkdir projects

        git clone https://gitlab.com/Sosnitskiy/mswordhighlightword.git

   5. change folder 

        cd mswordhighlightword
   6. Install all requirements 

        pip install requirements.txt

   7. run program 

    python main.py -f Test1_input -p Test1_input\GC_Dicts.xlsx -l DEBUG -g Test1_output
        



Available Options

optional arguments:

  -i INPUT_FILE, --input_file INPUT_FILE

                        Enter the path of the file to process

  -o OUTPUT_FILE, --output_file OUTPUT_FILE

                        Enter a valid output file

  -f INPUT_FOLDER, --input_folder INPUT_FOLDER

                        Enter the path of the folder to process

  -u OUT_FOLDER, --out_folder OUT_FOLDER

                        Enter the path of the output folder to to store files

  -l LOG_LEVEL, --log_level LOG_LEVEL

                        Enter the logging level. can be

  -p PARAMS_XLSX_FILE, --params_xlsx_file PARAMS_XLSX_FILE

                        Enter the path to xlsx file with parameters

  -g OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER

                        Output folder for result files


for run code on execute:

    main.py -f <folder name> -p <test\GC_Dicts.xlsx> -g <output folder>

or 

    main.py -i <file name> -p <test\GC_Dicts.xlsx> -g <output folder>

or 

    main.py -i <file name> -o <output file name> -p <test\GC_Dicts.xlsx>

possible set colour:

      BLACK - Black color
      BLUE - Blue color
      BRIGHT_GREEN - Bright green color.
      DARK_BLUE - Dark blue color.
      DARK_RED - Dark red color.
      DARK_YELLOW - Dark yellow color.
      GRAY_25 - 25% shade of gray color.
      GRAY_50 - 50% shade of gray color.
      GREEN - Green color.
      PINK - Pink color.
      RED - Red color.
      TEAL - Teal color.
      TURQUOISE - Turquoise color.
      VIOLET - Violet color.
      WHITE - White color.
      YELLOW - Yellow color.
    