from utilites import BaseClass, toPDFColour
import fitz
import os
import logging


class PdfClass(BaseClass):

    def run(self):
        BLUE_COLOR = (0, 0, 1)
        comment_title = "Bot"
        comment_info = "derevo_text"

        self.doc = fitz.open(self.file_name)

        found_matches = 0
        # Iterate throughout the document pages
        for pg, page in enumerate(self.doc):
            pageID = pg + 1
            for param in self.parameters:
                matched_values = page.search_for(param, hit_max=20)
                found_matches += len(matched_values) if matched_values else 0

                # Loop through the matches values
                # item will contain the coordinates of the found text
                for item in matched_values:
                    # Enclose the found text with a bounding box
                    annot = page.add_rect_annot(item)
                    annot.set_border({"dashes": [2], "width": 1.2})

                    annot.set_colors({"stroke": toPDFColour(self.parameters[param][1])})

                    if not self.parameters[param][0] or self.parameters[param][0] == "" or self.parameters[param][0] == '""':
                        pass
                    else:
                        # Add comment to the found match
                        info = annot.info
                        info["title"] = comment_title
                        info["content"] = self.parameters[param][0]
                        info["subject"] = "Educative subject"
                        annot.set_info(info)

                        annot.update()

    def save(self, orig_name_file, file_name=None, output_folder=None):
        head, tail = os.path.split(orig_name_file)
        if not file_name:
            file_name = "dddd.docx"
        if output_folder:
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)
        else:
            output_folder = ""
        file_name = os.path.join(output_folder, tail)
        self.doc.save(file_name, garbage=3, deflate=True)
        self.doc.close()
        logging.debug("file save with name: '{}'".format(file_name))