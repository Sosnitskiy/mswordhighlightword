import argparse
import filetype
import logging
import os
from docx.enum.text import WD_COLOR_INDEX
from fitz.utils import getColor


def is_valid_file_path(path):
    """
    Validate the path inputted and make sure it is a file path of type PDF
    """
    if not path:
        raise ValueError(f"Invalid Path")
    ss = filetype.guess(path).mime
    if os.path.isfile(path) and ('pdf' in filetype.guess(path).mime or path.endswith('docx')):
        return path
    else:
        logging.error(f"Invalid Path to {path}")
        raise ValueError(f"Invalid Path {path}")


def is_valid_folder_path(path):
    """
    Validate the path inputted and make sure it is a folder path to inputs docx and pfd files
    """
    if not path:
        raise ValueError(f"Invalid Path")
    if os.path.isdir(path):
        return path
    else:
        logging.error(f"Invalid Path to folder {path}")
        raise ValueError(f"Invalid Path {path}")


def parse_args():
    """
    Get user command line parameters
    """
    parser = argparse.ArgumentParser(description="Available Options")

    parser.add_argument('-i'
                        , '--input_file'
                        , dest='input_file'
                        , type=is_valid_file_path
                        , help="Enter the path of the file to process")

    parser.add_argument('-o'
                        , '--output_file'
                        , dest='output_file'
                        , type=str
                        , help="Enter a valid output file")

    parser.add_argument('-f'
                        , '--input_folder'
                        , dest='input_folder'
                        , type=is_valid_folder_path
                        , help="Enter the path of the folder to process")
    parser.add_argument('-u'
                        , '--out_folder'
                        , dest='out_folder'
                        , type=str
                        , help="Enter the path of the output folder to to store files")

    parser.add_argument('-l'
                        , '--log_level'
                        , dest='log_level'
                        , type=str
                        , help="Enter the logging level. can be ")

    parser.add_argument('-p'
                        , '--params_xlsx_file'
                        , dest='params_xlsx_file'
                        , type=str
                        , required=True
                        , help="Enter the path to xlsx file with parameters")

    parser.add_argument('-g'
                        , '--output_folder'
                        , dest='output_folder'
                        , type=str
                        , required=True
                        , help="Output folder for result files")

    args = vars(parser.parse_args())

    # To Display The Command Line Arguments
    logging.debug("## Command Arguments #################################################")
    logging.debug("\n".join("{}:{}".format(i, j) for i, j in args.items()))
    logging.debug("######################################################################")

    return args


class BaseClass:
    def __init__(self, file, parameters):
        self.file_name = file
        self.parameters = parameters
        self.doc = None

    def run(self):
        pass

    def save(self, file_name=None):
        pass

def toWDColour(colour):
    res_colour = WD_COLOR_INDEX.YELLOW
    if colour:
        if colour.upper() == 'BLACK':
            res_colour = WD_COLOR_INDEX.BLACK
        elif colour.upper() == 'BLUE':
            res_colour = WD_COLOR_INDEX.BLUE
        elif colour.upper() == 'BRIGHT_GREEN':
            res_colour = WD_COLOR_INDEX.BRIGHT_GREEN
        elif colour.upper() == 'DARK_BLUE':
            res_colour = WD_COLOR_INDEX.DARK_BLUE
        elif colour.upper() == 'DARK_RED':
            res_colour = WD_COLOR_INDEX.DARK_RED
        elif colour.upper() == 'DARK_YELLOW':
            res_colour = WD_COLOR_INDEX.DARK_YELLOW
        elif colour.upper() == 'GRAY_25':
            res_colour = WD_COLOR_INDEX.GRAY_25
        elif colour.upper() == 'GRAY_50':
            res_colour = WD_COLOR_INDEX.GRAY_50
        elif colour.upper() == 'GREEN':
            res_colour = WD_COLOR_INDEX.GREEN
        elif colour.upper() == 'PINK':
            res_colour = WD_COLOR_INDEX.PINK
        elif colour.upper() == 'RED':
            res_colour = WD_COLOR_INDEX.RED
        elif colour.upper() == 'TEAL':
            res_colour = WD_COLOR_INDEX.TEAL
        elif colour.upper() == 'TURQUOISE':
            res_colour = WD_COLOR_INDEX.TURQUOISE
        elif colour.upper() == 'VIOLET':
            res_colour = WD_COLOR_INDEX.VIOLET
        elif colour.upper() == 'WHITE':
            res_colour = WD_COLOR_INDEX.WHITE
        elif colour.upper() == 'YELLOW':
            res_colour = WD_COLOR_INDEX.YELLOW

    return res_colour

def toPDFColour(colour):
    res_colour = (0, 0, 1)
    if colour:
        if colour.upper() == 'BLACK':
            res_colour = getColor("aliceblue")
        elif colour.upper() == 'BLUE':
            res_colour = getColor("BLUE")
        elif colour.upper() == 'BRIGHT_GREEN':
            res_colour = getColor("GREEN4")
        elif colour.upper() == 'DARK_BLUE':
            res_colour = getColor("POWDERBLUE")
        elif colour.upper() == 'DARK_RED':
            res_colour = getColor("DARKRED")
        elif colour.upper() == 'DARK_YELLOW':
            res_colour = getColor("GREENYELLOW")
        elif colour.upper() == 'GRAY_25':
            res_colour = getColor("LIGHTGRAY")
        elif colour.upper() == 'GRAY_50':
            res_colour = getColor("SLATEGRAY")
        elif colour.upper() == 'GREEN':
            res_colour = getColor("GREEN")
        elif colour.upper() == 'PINK':
            res_colour = getColor("PINK")
        elif colour.upper() == 'RED':
            res_colour = getColor("RED")
        elif colour.upper() == 'TEAL':
            res_colour = getColor("DARKSALMON")
        elif colour.upper() == 'TURQUOISE':
            res_colour = getColor("MEDIUMTURQUOISE")
        elif colour.upper() == 'VIOLET':
            res_colour = getColor("VIOLET")
        elif colour.upper() == 'WHITE':
            res_colour = getColor("WHITE")
        elif colour.upper() == 'YELLOW':
            res_colour = getColor("YELLOW")

    return res_colour