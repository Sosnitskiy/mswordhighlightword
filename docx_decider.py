import datetime
import logging

from utilites import BaseClass, toWDColour
from docx import Document
import re
import os
from docx.enum.text import WD_COLOR_INDEX
import copy


class DocxClass(BaseClass):

    def run(self):
        self.doc = Document(self.file_name)
        for parag in self.doc.paragraphs:
            for param in self.parameters:
                patern = re.compile(param, re.IGNORECASE)
                num_p = 0
                comment_flg = True
                for num, run in reversed(list(enumerate(parag.runs))):
                    regxp_search_res = patern.search(run.text)
                    if regxp_search_res:
                        self._add_highlight(run, regxp_search_res, param, comment_flg, self.parameters[param][0], toWDColour(self.parameters[param][1]))
                        comment_flg = False
                    num_p += 1

    def _add_highlight(self, run, regxp_search_res, param, comment_flg, comment, colour=WD_COLOR_INDEX.DARK_YELLOW):
        data_text = run.text
        i1 = regxp_search_res.start()
        i2 = regxp_search_res.end()
        pre = data_text[:i1]
        post = data_text[i2:]
        text_param = data_text[i1:i2]

        logging.debug("first part: '" + pre + "'")
        logging.debug("highlight: '" + text_param + "'")
        logging.debug("last part: '" + post + "'")

        #create copy for have some formate
        pre_copy_run = copy.deepcopy(run)
        post_copy_run = copy.deepcopy(run)

        pre_copy_run.text = pre

        #highlight text
        rPr = run._r.get_or_add_rPr()
        rPr.highlight_val = colour
        run.text = text_param
        run._element.addprevious(pre_copy_run._r)

        run._element.addnext(post_copy_run._r)
        post_copy_run.text = post
        if comment_flg:
            self._add_comments(run, comment)


    def _add_comments(self, run, comment):
        if not comment or comment == "":
            pass
        else:
            run.add_comment(comment, "Bot", "{}".format(datetime.date.today()))

    def save(self, orig_name_file, file_name=None, output_folder=None):
        head, tail = os.path.split(orig_name_file)
        if not file_name:
            file_name = "dddd.docx"
        if output_folder:
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)
        else:
            output_folder = ""
        file_name = os.path.join(output_folder, tail)
        self.doc.save(file_name)
        logging.debug("file save with name: '{}'".format(file_name))
